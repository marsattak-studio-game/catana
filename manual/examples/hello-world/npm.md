<div align="center">

# Catana - NPM - Hello World

![catane-logo](../../../src/assets/img/catana-logo.png)

**[Go Home](../../../README.md)**

</div>

---

## `index.js`

```js
import { CatGameEngine, CatText } from '@msgame/catana';

window.addEventListener('load', () => {
  const gameEngine = new CatGameEngine();

  const text = new CatText({
    text: 'Hello World',
    color: 'black',
    x: 50,
    y: 50,
  });

  gameEngine.loop(draw => {
    draw(text);
  });

  gameEngine.run();
});
```
