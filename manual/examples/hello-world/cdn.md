 <div align="center">

# Catana - CDN - Hello World

![catane-logo](../../../src/assets/img/catana-logo.png)

**[Go Home](../../../README.md)**

</div>

---

</div>

## `index.html`

```html
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8" />
    <style>
      html,
      body {
        height: 100%;
        padding: 0;
        margin: 0;
      }
    </style>
    <title>Catana - CDN - Hello World</title>
  </head>
  <body>
    <div class="mobile-control"></div>
    <script
      type="text/javascript"
      src="https://unpkg.com/catana@latest"
    ></script>
    <script type="text/javascript" src="./index.js"></script>
  </body>
</html>
```

## `index.js`

```js
window.addEventListener('load', () => {
  const gameEngine = new Catana.CatGameEngine();

  const text = new Catana.CatText({
    text: 'Hello World',
    color: 'black',
    x: 50,
    y: 50,
  });

  gameEngine.loop(draw => {
    draw(text);
  });

  gameEngine.run();
});
```
