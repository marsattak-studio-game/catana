 <div align="center">

# Catana - CatImage Documentation

![catane-logo](../../src/assets/img/catana-logo.png)

**[Go Home](../../README.md)**

</div>

---

## CatImage

this class is a [ICatDrawableEntity](./CatGameEngine.md#icatdrawableentity).

It represents an image that can be drawn by the [CatGameEngine](./CatGameEngine.md).

### Methods

- [constructor](#constructor)
- [scale](#scale)
- [resetScale](#resetscale)
- [getX](#getx)
- [setX](#setx)
- [getY](#gety)
- [setY](#sety)
- [getWidth](#getwidth)
- [setWidth](#setwidth)
- [getHeight](#getheight)
- [setHeight](#setheight)
- [getRotation](#getrotation)
- [setRotation](#setrotation)

---

## constructor

`constructor(config: ICatImageConstructor)`

Create an instance of CatImage.

You can change the configuration with an [ICatImageConstructor](#icatimageconstructor) object.

### Example

```typescript
import { CatImage, ICatImageConstructor } from '@msgame/catana';

const config: ICatImageConstructor = {
  src:
    'https://gitlab.com/marsattak-studio-game/catana/-/raw/master/src/assets/img/catana-logo.png',
};

const image = new CatImage(config);
```

### ICatImageConstructor

This interface allows to define an object for the [CatImage constructor](#constructor).

| Name   | Description             | Type     | Required | Default                                                |
| ------ | ----------------------- | -------- | -------- | ------------------------------------------------------ |
| src    | image source            | `string` | yes      |                                                        |
| x      | X position of the image | `number` | no       | 0                                                      |
| y      | Y position of the image | `number` | no       | 0                                                      |
| width  | Width of the image      | `number` | no       | **(automatically detected size when image is loaded)** |
| height | Height of the image     | `number` | no       | **(automatically detected size when image is loaded)** |

---

## scale

`scale(value: number): void`

Scale the image.

### Example

```typescript
import { CatImage } from '@msgame/catana';

const image = new CatImage({
  src:
    'https://gitlab.com/marsattak-studio-game/catana/-/raw/master/src/assets/img/catana-logo.png',
});

image.getWidth(); // 200
image.scale(2);
image.getWidth(); // 400
```

---

## resetScale

`resetScale(): void`

Reset the scale to the default value (reinit size).

### Example

```typescript
import { CatImage } from '@msgame/catana';

const image = new CatImage({
  src:
    'https://gitlab.com/marsattak-studio-game/catana/-/raw/master/src/assets/img/catana-logo.png',
});

image.getWidth(); // 200
image.scale(2);
image.getWidth(); // 400
image.resetScale();
image.getWidth(); // 200
```

---

## getX

`getX(): number`

Return the X position of the image on the screen.

### Example

```typescript
import { CatImage } from '@msgame/catana';

const image = new CatImage({
  src:
    'https://gitlab.com/marsattak-studio-game/catana/-/raw/master/src/assets/img/catana-logo.png',
});

const xPosOfTheImage = image.getX();
```

---

## setX

`setX(x: number): void`

Set the X position of the image on the screen.

### Example

```typescript
import { CatImage } from '@msgame/catana';

const image = new CatImage({
  src:
    'https://gitlab.com/marsattak-studio-game/catana/-/raw/master/src/assets/img/catana-logo.png',
});

image.setX(100);
```

---

## getY

`getY(): number`

Return the Y position of the image on the screen.

### Example

```typescript
import { CatImage } from '@msgame/catana';

const image = new CatImage({
  src:
    'https://gitlab.com/marsattak-studio-game/catana/-/raw/master/src/assets/img/catana-logo.png',
});

const yPosOfTheImage = image.getY();
```

---

## setY

`setY(y: number): void`

Set the Y position of the image on the screen.

### Example

```typescript
import { CatImage } from '@msgame/catana';

const image = new CatImage({
  src:
    'https://gitlab.com/marsattak-studio-game/catana/-/raw/master/src/assets/img/catana-logo.png',
});

image.setY(100);
```

---

## getWidth

`getWidth(): number`

Return the Width of the image.

### Example

```typescript
import { CatImage } from '@msgame/catana';

const image = new CatImage({
  src:
    'https://gitlab.com/marsattak-studio-game/catana/-/raw/master/src/assets/img/catana-logo.png',
});

const widthOfTheImage = image.getWidth();
```

---

## setWidth

`setWidth(width: number): void`

Set the Width of the image.

### Example

```typescript
import { CatImage } from '@msgame/catana';

const image = new CatImage({
  src:
    'https://gitlab.com/marsattak-studio-game/catana/-/raw/master/src/assets/img/catana-logo.png',
});

image.setWidth(100);
```

---

## getHeight

`getHeight(): number`

Return the Height of the image.

### Example

```typescript
import { CatImage } from '@msgame/catana';

const image = new CatImage({
  src:
    'https://gitlab.com/marsattak-studio-game/catana/-/raw/master/src/assets/img/catana-logo.png',
});

const heightOfTheImage = image.getHeight();
```

---

## setHeight

`setHeight(height: number)`

Set the Height of the image.

### Example

```typescript
import { CatImage } from '@msgame/catana';

const image = new CatImage({
  src:
    'https://gitlab.com/marsattak-studio-game/catana/-/raw/master/src/assets/img/catana-logo.png',
});

image.setHeight(100);
```

---

## getRotation

`getRotation(): number`

Returns the rotation value of the image.

> **The return value ranges from 0 to 359.**

### Example

```typescript
import { CatImage } from '@msgame/catana';

const image = new CatImage({
  src:
    'https://gitlab.com/marsattak-studio-game/catana/-/raw/master/src/assets/img/catana-logo.png',
});

const rotationValueOfTheImage = image.getRotation();
```

---

## setRotation

`setRotation(rotation: number)`

Set the rotation value of the image.

### Example

```typescript
import { CatImage } from '@msgame/catana';

const image = new CatImage({
  src:
    'https://gitlab.com/marsattak-studio-game/catana/-/raw/master/src/assets/img/catana-logo.png',
});

image.setRotation(45);
```

---
