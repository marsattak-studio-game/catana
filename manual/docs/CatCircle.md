 <div align="center">

# Catana - CatCircle Documentation

![catane-logo](../../src/assets/img/catana-logo.png)

**[Go Home](../../README.md)**

</div>

---

## CatCircle

this class is a [ICatDrawableEntity](./CatGameEngine.md#icatdrawableentity).

It represents a circle that can be drawn by the [CatGameEngine](./CatGameEngine.md).

### Methods

- [constructor](#constructor)
- [scale](#scale)
- [resetScale](#resetscale)
- [getColor](#getcolor)
- [setColor](#setcolor)
- [getX](#getx)
- [setX](#setx)
- [getY](#gety)
- [setY](#sety)
- [getSize](#getsize)
- [setSize](#setsize)
- [getRotation](#getrotation)
- [setRotation](#setrotation)

---

## constructor

`constructor(config: ICatCircleConstructor)`

Create an instance of CatCircle.

You can change the configuration with an [ICatCircleConstructor](#icatcircleconstructor) object.

### Example

```typescript
import { CatCircle, ICatCircleConstructor } from '@msgame/catana';

const config: ICatCircleConstructor = {
  color: 'black',
  x: 50,
  y: 50,
  size: 150,
};

const circle = new CatCircle(config);
```

### ICatCircleConstructor

This interface allows to define an object for the [CatCircle constructor](#constructor).

| Name  | Description              | Type     | Required | Default |
| ----- | ------------------------ | -------- | -------- | ------- |
| color | circle color             | `string` | yes      |         |
| x     | X position of the circle | `number` | yes      |         |
| y     | Y position of the circle | `number` | yes      |         |
| size  | Size of the circle       | `number` | yes      |         |

---

## scale

`scale(value: number): void`

Scale the circle.

### Example

```typescript
import { CatCircle } from '@msgame/catana';

const circle = new CatCircle({
  color: 'black',
  x: 50,
  y: 50,
  size: 150,
});

circle.getSize(); // 150
circle.scale(2);
circle.getSize(); // 300
```

---

## resetScale

`resetScale(): void`

Reset the scale to the default value (reinit size).

### Example

```typescript
import { CatCircle } from '@msgame/catana';

const circle = new CatCircle({
  color: 'black',
  x: 50,
  y: 50,
  size: 150,
});

circle.getSize(); // 150
circle.scale(2);
circle.getSize(); // 300
circle.resetScale();
circle.getSize(); // 150
```

---

## getColor

`getColor(): string`

Return the color of the circle.

### Example

```typescript
import { CatCircle } from '@msgame/catana';

const circle = new CatCircle({
  color: 'black',
  x: 50,
  y: 50,
  size: 150,
});

const colorOfTheCircle = circle.getColor();
```

---

## setColor

`setColor(color: string): void`

Set the color of the circle.

### Example

```typescript
import { CatCircle } from '@msgame/catana';

const circle = new CatCircle({
  color: 'black',
  x: 50,
  y: 50,
  size: 150,
  height: 100,
});

circle.setColor('blue');
```

---

## getX

`getX(): number`

Return the X position of the circle on the screen.

### Example

```typescript
import { CatCircle } from '@msgame/catana';

const circle = new CatCircle({
  color: 'black',
  x: 50,
  y: 50,
  size: 150,
});

const xPosOfTheCircle = circle.getX();
```

---

## setX

`setX(x: number): void`

Set the X position of the circle on the screen.

### Example

```typescript
import { CatCircle } from '@msgame/catana';

const circle = new CatCircle({
  color: 'black',
  x: 50,
  y: 50,
  size: 150,
});

circle.setX(100);
```

---

## getY

`getY(): number`

Return the Y position of the circle on the screen.

### Example

```typescript
import { CatCircle } from '@msgame/catana';

const circle = new CatCircle({
  color: 'black',
  x: 50,
  y: 50,
  size: 150,
});

const yPosOfTheCircle = circle.getY();
```

---

## setY

`setY(y: number): void`

Set the Y position of the circle on the screen.

### Example

```typescript
import { CatCircle } from '@msgame/catana';

const circle = new CatCircle({
  color: 'black',
  x: 50,
  y: 50,
  size: 150,
});

circle.setY(100);
```

---

## getSize

`getSize(): number`

Returns the size of the circle.

### Example

```typescript
import { CatCircle } from '@msgame/catana';

const circle = new CatCircle({
  color: 'black',
  x: 50,
  y: 50,
  size: 150,
});

const sizeOfTheCircle = circle.getSize();
```

---

## setSize

`setSize(width: number): void`

Set the size of the circle.

### Example

```typescript
import { CatCircle } from '@msgame/catana';

const circle = new CatCircle({
  color: 'black',
  x: 50,
  y: 50,
  size: 150,
});

circle.setSize(100);
```

---

## getRotation

`getRotation(): number`

Returns the rotation value of the circle.

> **The return value ranges from 0 to 359.**

### Example

```typescript
import { CatCircle } from '@msgame/catana';

const circle = new CatCircle({
  color: 'black',
  x: 50,
  y: 50,
  size: 150,
});

const rotationValueOfTheCircle = circle.getRotation();
```

---

## setRotation

`setRotation(rotation: number)`

Set the rotation value of the circle.

### Example

```typescript
import { CatCircle } from '@msgame/catana';

const circle = new CatCircle({
  color: 'black',
  x: 50,
  y: 50,
  size: 150,
});

circle.setRotation(45);
```

---
