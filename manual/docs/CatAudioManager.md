 <div align="center">

# Catana - CatAudioManager Documentation

![catane-logo](../../src/assets/img/catana-logo.png)

**[Go Home](../../README.md)**

</div>

---

## CatAudioManager

The CatAudioManager class allows to manage the different sounds.

### Methods

- [constructor](#constructor)
- [load](#load)
- [playSound](#playdound)
- [stopSound](#stopsound)

---

## constructor

`constructor()`

Create an instance of CatAudioManager.

### Example

```typescript
import { CatAudioManager } from '@msgame/catana';

const audioManager = new CatAudioManager();
```

> With typescript you can specify name types for the sounds.

```typescript
type IMySoundName = 'sound_1' | 'sound_2';
const audioManager = new CatAudioManager<IMySoundName>();
```

---

## load

`load(audioSources: ICatAudioManagerAudioToLoad<ISoundName>[]): Promise<void>`

This function allows you to load sounds.

It **returns** a resolved promise once all sounds are loaded or reject if there was an error.

| Name         | Description            | Type                                        | Required | Default |
| ------------ | ---------------------- | ------------------------------------------- | -------- | ------- |
| audioSources | List of sounds to load | `ICatAudioManagerAudioToLoad<ISoundName>[]` | yes      |         |

### Example

```typescript
import { CatAudioManager } from '@msgame/catana';

const audioManager = new CatAudioManager();

audioMgr
  .load([
    {
      name: 'sound_1',
      src: './sound_1.mp3',
    },
  ])
  .then(() => {
    // Sounds loaded
  })
  .catch(error => {
    // Error
  });
```

---

## playSound

`playSound(name: ISoundName): void`

This function allows you to play a sound.

| Name | Description            | Type                  | Required | Default |
| ---- | ---------------------- | --------------------- | -------- | ------- |
| name | The name of the sound. | `ISoundName | string` | yes      |         |

### Example

```typescript
import { CatAudioManager } from '@msgame/catana';

const audioManager = new CatAudioManager();

audioMgr
  .load([
    {
      name: 'sound_1',
      src: './sound_1.mp3',
    },
  ])
  .then(() => {
    audioManager.playSound('sound_1');
  });
```

---

## stopSound

`stopSound(name: ISoundName, smooth: number = 0): Promise<void>`

This function allows to stop a sound.

It returns a resolved promise once the sound is finished or reject if there was an error.

| Name   | Description                                                            | Type                  | Required | Default |
| ------ | ---------------------------------------------------------------------- | --------------------- | -------- | ------- |
| name   | The name of the sound to be stopped.                                   | `ISoundName | string` | yes      |         |
| smooth | Allows you to gradually lower the volume before turning off the sound. | `number`              | no       | 0       |

### Example

```typescript
import { CatAudioManager } from '@msgame/catana';

const audioManager = new CatAudioManager();

audioMgr
  .load([
    {
      name: 'sound_1',
      src: './sound_1.mp3',
    },
  ])
  .then(() => {
    audioManager.playSound('sound_1');
    audioManager.stopSound('sound_1', 1000);
  });
```

---
