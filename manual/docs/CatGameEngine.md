 <div align="center">

# Catana - CatGameEngine Documentation

![catane-logo](../../src/assets/img/catana-logo.png)

**[Go Home](../../README.md)**

</div>

---

## CatGameEngine

The CatGameEngine class is the one who's going to manage the whole application.

### Methods

- [constructor](#constructor)
- [loop](#loop)
- [run](#run)
- [keyboardEvent](#keyboardevent)
- [mouseEvent](#mouseevent)
- [tactilEvent](#tactilevent)
- [setScene](#setscene)
- [getInitialScreenWidth](#getinitialscreenwidth)
- [getInitialScreenHeight](#getinitialscreenheight)
- [getScreenWidth](#getscreenwidth)
- [getScreenHeight](#getscreenheight)
- [scaleScreen](#scalescreen)

---

## constructor

`constructor(config: ICatGameEngineConstructor = {})`

Create an instance of CatGameEngine.

You can change the configuration with an [ICatGameEngineConstructor](#icatgameengineconstructor) object.

### Example

```typescript
import { CatGameEngine } from '@msgame/catana';

const gameEngine = new CatGameEngine();
```

### ICatGameEngineConstructor

This interface allows to define an object for the [CatGameEngine constructor](#constructor).

| Name                | Description                            | Type      | Required | Default         |
| ------------------- | -------------------------------------- | --------- | -------- | --------------- |
| debugMode           | enable/disable debug mode              | `boolean` | no       | false           |
| containerId         | the id of DOM container for the screen | `string`  | no       | 'catana-screen' |
| initialScreenWidth  | the initial screen width               | `number`  | no       | 1280            |
| initialScreenHeight | the initial screen height              | `number`  | no       | 768             |

---

## loop

`loop(callback: ({ draw, measureText }: ICatGameEngineLoopAgs) => void): void`

This function allows to define the function of the main infiny loop.

The callback function provides us with an object in parameter: [ICatGameEngineLoopAgs](#icatgameengineloopargs).

| Name     | Description                        | Type                                                     | Required | Default |
| -------- | ---------------------------------- | -------------------------------------------------------- | -------- | ------- |
| callback | function to be added to the engine | `({ draw, measureText }: ICatGameEngineLoopAgs) => void` | yes      |         |

### Example

```typescript
import { CatGameEngine } from '@msgame/catana';

const gameEngine = new CatGameEngine();

gameEngine.loop(() => {
  // Here, you can draw and update your entities
});

gameEngine.run();
```

### ICatGameEngineLoopAgs

Parameter of the loop callback.

| Name        | Description                                                  | Type                                   |
| ----------- | ------------------------------------------------------------ | -------------------------------------- |
| draw        | function to draw a [ICatDrawableEntity](#icatdrawableentity) | `(entity: ICatDrawableEntity) => void` |
| measureText | get dimension of a [CatText](./CatText.md)                   | `(text: CatText) => TextMetrics`       |

### ICatDrawableEntity

List of objects that can be drawn:

- [CatCircle](./CatCircle.md)
- [CatImage](./CatImage.md)
- [CatRect](./CatRect.md)
- [CatSprite](./CatSprite.md)
- [CatText](./CatText.md)

### Example

```typescript
import { CatGameEngine, CatText, ICatGameEngineLoopAgs } from '@msgame/catana';

const gameEngine = new CatGameEngine();

const text = new CatText({
  text: 'Hello ',
  x: 0,
  y: 0,
  color: 'black',
});

const text2 = new CatText({
  text: 'world !',
  x: 0,
  y: 0,
  color: 'red',
});

gameEngine.loop(({ draw, measureText }: ICatGameEngineLoopAgs) => {
  draw(text);
  const textWidth = measureText(text).width;
  text2.setX(textWidth);
  draw(text2);
});

gameEngine.run();
```

---

## run

`run(): void`

This function allows you to launch the application.

### Example

```typescript
import { CatGameEngine } from '@msgame/catana';

const gameEngine = new CatGameEngine();

gameEngine.run();
```

---

## keyboardEvent

`keyboardEvent(callback: (event: KeyboardEvent, type: IKeyboardEventType) => void): void`

This function allows you to capture keyboard events.

| Name     | Description           | Type                                                       | Required | Default |
| -------- | --------------------- | ---------------------------------------------------------- | -------- | ------- |
| callback | function to get event | `(event: KeyboardEvent, type: IKeyboardEventType) => void` | yes      |         |

It takes in parameter the event and its type.

| Name  | Description                                                            | Type                 |
| ----- | ---------------------------------------------------------------------- | -------------------- |
| event | the event                                                              | `KeyboardEvent`      |
| type  | The event type [IKeyboardEventType](./CatAScene.md#ikeyboardeventtype) | `IKeyboardEventType` |

### Example

```typescript
import { CatGameEngine } from '@msgame/catana';

const gameEngine = new CatGameEngine();

gameEngine.keyboardEvent((event, type) => {
  console.log('keyboardEvent', event, type);
});

gameEngine.run();
```

---

## mouseEvent

`mouseEvent( callback: (event: MouseEvent, type: IMouseEventType) => void ): void`

This function allows you to capture mouse events.

| Name     | Description           | Type                                                 | Required | Default |
| -------- | --------------------- | ---------------------------------------------------- | -------- | ------- |
| callback | function to get event | `(event: MouseEvent, type: IMouseEventType) => void` | yes      |         |

It takes in parameter the event and its type.

| Name  | Description                                                      | Type              |
| ----- | ---------------------------------------------------------------- | ----------------- |
| event | the event                                                        | `MouseEvent`      |
| type  | The event type [IMouseEventType](./CatAScene.md#imouseeventtype) | `IMouseEventType` |

### Example

```typescript
import { CatGameEngine } from '@msgame/catana';

const gameEngine = new CatGameEngine();

gameEngine.mouseEvent((event, type) => {
  console.log('mouseEvent', event, type);
});

gameEngine.run();
```

---

## tactilEvent

`tactilEvent(callback: (event: TouchEvent, type: ITouchEventType) => void): void`

This function allows you to capture tactil events.

| Name     | Description           | Type                                                 | Required | Default |
| -------- | --------------------- | ---------------------------------------------------- | -------- | ------- |
| callback | function to get event | `(event: TouchEvent, type: ITouchEventType) => void` | yes      |         |

It takes in parameter the event and its type.

| Name  | Description                                                      | Type              |
| ----- | ---------------------------------------------------------------- | ----------------- |
| event | the event                                                        | `KeyboardEvent`   |
| type  | The event type [ITouchEventType](./CatAScene.md#itoucheventtype) | `ITouchEventType` |

### Example

```typescript
import { CatGameEngine } from '@msgame/catana';

const gameEngine = new CatGameEngine();

gameEngine.tactilEvent((event, type) => {
  console.log('tactilEvent', event, type);
});

gameEngine.run();
```

---

## setScene

`setScene(scene: CatAScene): void`

This function allows to define the scene ([CatAScene](./CatAScene.md)).

### Example

```typescript
import { CatGameEngine, CatAScene } from '@msgame/catana';

const gameEngine = new CatGameEngine();

class MyScene extends CatAScene {
  loop() {
    console.log('loop');
  }
  keyboardEvent() {
    console.log('keyboardEvent');
  }
  mouseEvent() {
    console.log('mouseEvent');
  }
  tactilEvent() {
    console.log('tactilEvent');
  }
}

gameEngine.setScene(new MyScene(gameEngine));
```

---

## getInitialScreenWidth

`getInitialScreenWidth(): number`

This function return the initial screen width.

### Example

```typescript
import { CatGameEngine } from '@msgame/catana';

const gameEngine = new CatGameEngine();

const initialScreenWidth = gameEngine.getInitialScreenWidth();
```

---

## getInitialScreenHeight

`getInitialScreenHeight(): number`

This function return the initial screen height.

### Example

```typescript
import { CatGameEngine } from '@msgame/catana';

const gameEngine = new CatGameEngine();

const initialScreenHeight = gameEngine.getInitialScreenHeight();
```

---

## getScreenWidth

`getScreenWidth(): number`

This function return the actual screen width.

### Example

```typescript
import { CatGameEngine } from '@msgame/catana';

const gameEngine = new CatGameEngine();

const screenWidth = gameEngine.getScreenWidth();
```

---

## getScreenHeight

`getScreenHeight(): number`

This function return the actual screen height.

### Example

```typescript
import { CatGameEngine } from '@msgame/catana';

const gameEngine = new CatGameEngine();

const screenHeight = gameEngine.getScreenHeight();
```

---

---

## scaleScreen

`scaleScreen(scaleX: number, scaleY?: number): void`

This function allows you to scale the screen.

You can specify the scaling on the x and y axis.

You can also specify only one value, which will be used for the x and y axis.

You can specify negative values for horizontal or vertical flipping.

### Example

```typescript
import { CatGameEngine } from '@msgame/catana';

const gameEngine = new CatGameEngine();

gameEngine.scaleScreen(2, 2);

// Is equal to:

gameEngine.scaleScreen(2);
```

---
