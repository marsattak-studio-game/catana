 <div align="center">

# Catana - CatSprite Documentation

![catane-logo](../../src/assets/img/catana-logo.png)

**[Go Home](../../README.md)**

</div>

---

## CatSprite extends [CatImage](./CatImage.md)

this class is a [ICatDrawableEntity](./CatGameEngine.md#icatdrawableentity).

It represents a sprite that can be drawn by the [CatGameEngine](./CatGameEngine.md).

it can define the section of image to draw.

### Methods

- [...CatImage methods](./CatImage.md#methods)
- [constructor](#constructor)
- [getSpriteSection](#getspritesection)
- [setSpriteSection](#setspritesection)

---

## constructor

`constructor(config: ICatSpriteConstructor)`

Create an instance of CatSprite.

You can change the configuration with an [ICatSpriteConstructor](#icatspriteconstructor) object.

### Example

```typescript
import { CatSprite, ICatSpriteConstructor } from '@msgame/catana';

const config: ICatSpriteConstructor = {
  src:
    'https://gitlab.com/marsattak-studio-game/catana/-/raw/master/src/assets/img/catana-logo.png',
};

const sprite = new CatSprite(config);
```

### ICatSpriteConstructor

This interface allows to define an object for the [CatSprite constructor](#constructor).

| Name                                                          | Description                          | Type                   | Required | Default                                                |
| ------------------------------------------------------------- | ------------------------------------ | ---------------------- | -------- | ------------------------------------------------------ |
| ...[ICatImageConstructor](./CatImage.md#caatimageconstructor) | ICatImageConstructor config          | `ICatImageConstructor` | Required |                                                        |
| xSection                                                      | x section of the sprite to draw      | `number`               | no       | 0                                                      |
| ySection                                                      | y section of the sprite to draw      | `number`               | no       | 0                                                      |
| widthSection                                                  | width section of the sprite to draw  | `number`               | no       | **(automatically detected size when image is loaded)** |
| heightSection                                                 | height section of the sprite to draw | `number`               | no       | **(automatically detected size when image is loaded)** |

---

## getSpriteSection

`getSpriteSection(): ICatSpriteSection`

Return the sprite section([ICatSpriteSection](#icatspritesection)) of the image to draw.

### Example

```typescript
import { CatSprite } from '@msgame/catana';

const sprite = new CatSprite({
  src:
    'https://gitlab.com/marsattak-studio-game/catana/-/raw/master/src/assets/img/catana-logo.png',
});

const spriteSectionOfTheImage = sprite.getSpriteSection();
```

---

## setSpriteSection

`setSpriteSection(section: ICatSpriteSection)`

Set the sprite section([ICatSpriteSection](#icatspritesection)) of the image to draw.

### Example

```typescript
import { CatSprite } from '@msgame/catana';

const sprite = new CatSprite({
  src:
    'https://gitlab.com/marsattak-studio-game/catana/-/raw/master/src/assets/img/catana-logo.png',
});

sprite.setSpriteSection({
  x: 0,
  y: 0,
  width: 64,
  height: 64,
});
```

### ICatSpriteSection

This interface allows to define the sprite section of the image to draw.

| Name   | Description                          | Type     | Required | Default |
| ------ | ------------------------------------ | -------- | -------- | ------- |
| x      | x section of the sprite to draw      | `number` | yes      |         |
| y      | y section of the sprite to draw      | `number` | yes      |         |
| width  | width section of the sprite to draw  | `number` | yes      |         |
| height | height section of the sprite to draw | `number` | yes      |         |

---
