 <div align="center">

# Catana - CatAScene Documentation

![catane-logo](../../src/assets/img/catana-logo.png)

**[Go Home](../../README.md)**

</div>

---

## CatAScene

The **abstract** class CatAScene will allow to implement a custom scene.

> These are the same functions used when implementing via the game engine,
> i.e. adding its functions via the game engine or via a CatAScene class will overwrite the other one.

### Methods

- [constructor](#constructor)
- [getGameEngine](#scale)

#### Abstract

- [loop](#resetscale)
- [keyboardEvent](#getcolor)
- [mouseEvent](#setcolor)
- [tactilEvent](#getx)

---

## constructor

`constructor(ge: CatGameEngine)`

Create an instance of CatCircle.

Takes as parameter an instance of the [CatGameEngine](./CatGameEngine).

```typescript
import { CatGameEngine, CatAScene } from '@msgame/catana';

class MyScene extends CatAScene {
  constructor(gameEngine: CatGameEngine) {
    super(gameEngine);
  }
}
```

---

## getGameEngine

`getGameEngine(): CatGameEngine`

Return the instance of the [CatGameEngine](./CatGameEngine).

> this method is **protected** and can only be used within the class that implements CatAScene.

### Example

```typescript
import { CatGameEngine, CatAScene } from '@msgame/catana';

class MyScene extends CatAScene {
  constructor(gameEngine: CatGameEngine) {
    super(gameEngine);
    console.log(this.getGameEngine().getInitialScreenWidth());
  }
}
```

---

## loop

`loop({ draw, measureText }: ICatGameEngineLoopAgs): void`

This method allows you to define the function of the main infiny loop.

> this method is **protected** and can only be used within the class that implements CatAScene.

The method provides us with an object in parameter: [ICatGameEngineLoopAgs](./CatGameEngine.md#icatgameengineloopargs).

### Example

```typescript
import { CatGameEngine, CatAScene } from '@msgame/catana';

class MyScene extends CatAScene {
  constructor(gameEngine: CatGameEngine) {
    super(gameEngine);
  }

  loop({ draw, measureText }) {
    // Here, you can draw and update your entities
  }
}
```

---

## keyboardEvent

`keyboardEvent(event: KeyboardEvent, type: IKeyboardEventType): void`

This method allows you to capture keyboard events.

> this method is **abstracted** and must obligatorily be implemented in the class that inherits from CatAScene.

It takes in parameter the event and its type.

| Name  | Description                                              | Type                 |
| ----- | -------------------------------------------------------- | -------------------- |
| event | the event                                                | `KeyboardEvent`      |
| type  | The event type [IKeyboardEventType](#ikeyboardeventtype) | `IKeyboardEventType` |

### IKeyboardEventType

`type IKeyboardEventType = 'keyup' | 'keydown' | 'keydown';`

### Example

```typescript
import { CatGameEngine, CatAScene } from '@msgame/catana';

class MyScene extends CatAScene {
  constructor(gameEngine: CatGameEngine) {
    super(gameEngine);
  }

  keyboardEvent(event, type) {
    console.log('keyboardEvent', event, type);
  }
}
```

---

## mouseEvent

`mouseEvent(event: MouseEvent, type: IMouseEventType): void`

This method allows you to capture mouse events.

> this method is **abstracted** and must obligatorily be implemented in the class that inherits from CatAScene.

It takes in parameter the event and its type.

| Name  | Description                                        | Type              |
| ----- | -------------------------------------------------- | ----------------- |
| event | the event                                          | `MouseEvent`      |
| type  | The event type [IMouseEventType](#imouseeventtype) | `IMouseEventType` |

### IMouseEventType

`type IMouseEventType = 'mouseup' | 'mousedown' | 'mousemove';`

### Example

```typescript
import { CatGameEngine, CatAScene } from '@msgame/catana';

class MyScene extends CatAScene {
  constructor(gameEngine: CatGameEngine) {
    super(gameEngine);
  }

  mouseEvent(event, type) {
    console.log('mouseEvent', event, type);
  }
}
```

---

## tactilEvent

`tactilEvent(event: TouchEvent, type: ITouchEventType): void`

This method allows you to capture tactil events.

> this method is **abstracted** and must obligatorily be implemented in the class that inherits from CatAScene.

It takes in parameter the event and its type.

| Name  | Description                                        | Type              |
| ----- | -------------------------------------------------- | ----------------- |
| event | the event                                          | `KeyboardEvent`   |
| type  | The event type [ITouchEventType](#itoucheventtype) | `ITouchEventType` |

### ITouchEventType

`type ITouchEventType = 'touchend' | 'touchstart' | 'touchmove';`

### Example

```typescript
import { CatGameEngine, CatAScene } from '@msgame/catana';

class MyScene extends CatAScene {
  constructor(gameEngine: CatGameEngine) {
    super(gameEngine);
  }

  tactilEvent(event, type) {
    console.log('tactilEvent', event, type);
  }
}
```

---
