 <div align="center">

# Catana - CatText Documentation

![catane-logo](../../src/assets/img/catana-logo.png)

**[Go Home](../../README.md)**

</div>

---

## CatText

this class is a [ICatDrawableEntity](./CatGameEngine.md#icatdrawableentity).

It represents a text that can be drawn by the [CatGameEngine](./CatGameEngine.md).

### Methods

- [constructor](#constructor)
- [getColor](#getcolor)
- [setColor](#setcolor)
- [getAlpha](#getalpha)
- [setAlpha](#setalpha)
- [getX](#getx)
- [setX](#setx)
- [getY](#gety)
- [setY](#sety)
- [getRotation](#getrotation)
- [setRotation](#setrotation)
- [getText](#gettext)
- [setText](#settext)
- [getFont](#getfont)
- [setFont](#setfont)
- [getSize](#getsize)
- [setSize](#setsize)
- [getShadow](#getshadow)
- [setShadow](#setshadow)

---

## constructor

`constructor(config: ICatTextConstructor)`

Create an instance of CatText.

You can change the configuration with an [ICatTextConstructor](#icattextconstructor) object.

### Example

```typescript
import { CatText, ICatTextConstructor } from '@msgame/catana';

const config: ICatTextConstructor = {
  text: 'Hello world',
  color: 'black',
  x: 50,
  y: 50,
};

const text = new CatText(config);
```

### ICatTextConstructor

This interface allows to define an object for the [CatText constructor](#constructor).

| Name          | Description                 | Type     | Required | Default |
| ------------- | --------------------------- | -------- | -------- | ------- |
| text          | text                        | `string` | yes      |         |
| color         | text color                  | `string` | yes      |         |
| x             | X position of the text      | `number` | yes      |         |
| y             | Y position of the text      | `number` | yes      |         |
| size          | size of the text            | `string` | no       | '40px'  |
| alpha         | transparence of the text    | `number` | no       | 1       |
| offsetXShadow | x offset shadow of the text | `number` | no       | 0       |
| offsetYShadow | y offset shadow of the text | `number` | no       | 0       |
| colorShadow   | shadow color of the text    | `string` | no       | 'black' |
| font          | font of the text            | `string` | no       | 'serif' |

---

## getColor

`getColor(): string`

Return the color of the text.

### Example

```typescript
import { CatText } from '@msgame/catana';

const text = new CatText({
  text: 'Hello world',
  color: 'black',
  x: 50,
  y: 50,
});

const colorOfTheText = text.getColor();
```

---

## setColor

`setColor(color: string): void`

Set the color of the text.

### Example

```typescript
import { CatText } from '@msgame/catana';

const text = new CatText({
  text: 'Hello world',
  color: 'black',
  x: 50,
  y: 50,
});

text.setColor('blue');
```

---

## getAlpha

`getAlpha(): number`

Return alpha of the text.

### Example

```typescript
import { CatText } from '@msgame/catana';

const text = new CatText({
  text: 'Hello world',
  color: 'black',
  x: 50,
  y: 50,
});

const alphaOfTheText = text.getAlpha();
```

---

## setAlpha

`setAlpha(x: number): void`

Set alpha of the text.

### Example

```typescript
import { CatText } from '@msgame/catana';

const text = new CatText({
  text: 'Hello world',
  color: 'black',
  x: 50,
  y: 50,
});

text.setAlpha(0.5);
```

---

## getX

`getX(): number`

Return the X position of the text on the screen.

### Example

```typescript
import { CatText } from '@msgame/catana';

const text = new CatText({
  text: 'Hello world',
  color: 'black',
  x: 50,
  y: 50,
});

const xPosOfTheText = text.getX();
```

---

## setX

`setX(x: number): void`

Set the X position of the text on the screen.

### Example

```typescript
import { CatText } from '@msgame/catana';

const text = new CatText({
  text: 'Hello world',
  color: 'black',
  x: 50,
  y: 50,
});

text.setX(100);
```

---

## getY

`getY(): number`

Return the Y position of the text on the screen.

### Example

```typescript
import { CatText } from '@msgame/catana';

const text = new CatText({
  text: 'Hello world',
  color: 'black',
  x: 50,
  y: 50,
});

const yPosOfTheText = text.getY();
```

---

## setY

`setY(y: number): void`

Set the Y position of the text on the screen.

### Example

```typescript
import { CatText } from '@msgame/catana';

const text = new CatText({
  text: 'Hello world',
  color: 'black',
  x: 50,
  y: 50,
});

text.setY(100);
```

---

## getRotation

`getRotation(): number`

Returns the rotation value of the text on the screen.

> **The return value ranges from 0 to 359.**

### Example

```typescript
import { CatText } from '@msgame/catana';

const text = new CatText({
  text: 'Hello world',
  color: 'black',
  x: 50,
  y: 50,
});

const rotationValueOfTheText = text.getRotation();
```

---

## setRotation

`setRotation(rotation: number): void`

Set the rotation value of the text on the screen.

### Example

```typescript
import { CatText } from '@msgame/catana';

const text = new CatText({
  text: 'Hello world',
  color: 'black',
  x: 50,
  y: 50,
});

text.setRotation(45);
```

---

## getText

`getText(): string`

Return the value of the text.

### Example

```typescript
import { CatText } from '@msgame/catana';

const text = new CatText({
  text: 'Hello world',
  color: 'black',
  x: 50,
  y: 50,
});

const valueOfTheText = text.getText();
```

---

## setText

`setText(text: string): void`

Set the value of the text.

### Example

```typescript
import { CatText } from '@msgame/catana';

const text = new CatText({
  text: 'Hello world',
  color: 'black',
  x: 50,
  y: 50,
});

text.setText(100);
```

---

## getFont

`getFont(): string`

Return the font of the text.

### Example

```typescript
import { CatText } from '@msgame/catana';

const text = new CatText({
  text: 'Hello world',
  color: 'black',
  x: 50,
  y: 50,
});

const fontOfTheText = text.getFont();
```

---

## setFont

`setFont(font: string): void`

Set the font of the text.

### Example

```typescript
import { CatText } from '@msgame/catana';

const text = new CatText({
  text: 'Hello world',
  color: 'black',
  x: 50,
  y: 50,
});

text.setFont('serif');
```

---

## getSize

`getSize(): string`

Return the size of the text.

### Example

```typescript
import { CatText } from '@msgame/catana';

const text = new CatText({
  text: 'Hello world',
  color: 'black',
  x: 50,
  y: 50,
});

const sizeOfTheText = text.getSize();
```

---

## setSize

`setSize(size: string): void`

Set the size of the text.

### Example

```typescript
import { CatText } from '@msgame/catana';

const text = new CatText({
  text: 'Hello world',
  color: 'black',
  x: 50,
  y: 50,
});

text.setSize('20px');
```

---

## getShadow

`getShadow(): ITextShadow`

Return the shadow([ITextShadow](#itexshadow)) of the text.

### Example

```typescript
import { CatText } from '@msgame/catana';

const text = new CatText({
  text: 'Hello world',
  color: 'black',
  x: 50,
  y: 50,
});

const shadowOfTheText = text.getShadow();
```

---

## setShadow

`setShadow(shadow: ITextShadow): void`

Set the shadow([ITextShadow](#itexshadow)) of the text.

### Example

```typescript
import { CatText } from '@msgame/catana';

const text = new CatText({
  text: 'Hello world',
  color: 'black',
  x: 50,
  y: 50,
});

text.setShadow({
  offsetXShadow: 10,
  offsetYShadow: 10,
  colorShadow: 'red',
});
```

### ITextShadow

This interface allows to define a text shadow object

| Name          | Description                 | Type     | Required | Default |
| ------------- | --------------------------- | -------- | -------- | ------- |
| offsetXShadow | x offset shadow of the text | `number` | yes      |         |
| offsetYShadow | y offset shadow of the text | `number` | yes      |         |
| colorShadow   | shadow color of the text    | `string` | yes      |         |

---
