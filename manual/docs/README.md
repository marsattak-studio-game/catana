 <div align="center">

# Catana - Documentation

![catane-logo](../../src/assets/img/catana-logo.png)

**[Go Home](../../README.md)**

</div>

---

### API Reference

#### Classes

##### Abstract

- [CatAScene](#catascene)

##### Engine

- [CatGameEngine](#catgameengine)

##### Drawable

- [CatCircle](#catcircle)
- [CatImage](#catimage)
- [CatRect](#catrect)
- [CatSprite](#catsprite)
- [CatText](#cattext)

##### Audio

- [CatAudioManager](#cataudiomanager)

#### Constants

- [FONT_TYPE](#font_type)
- [FONT_SIZE](#font_size)

#### Functions

- [isTouchDevice](#istouchdevice)

---

## Abstract

---

#### CatAScene

The abstract class CatAScene will allow to implement a custom scene.

[Read more.](./CatAScene.md)

##### Example

```typescript
import { CatGameEngine, CatAScene } from '@msgame/catana';

const gameEngine = new CatGameEngine();

class MyScene extends CatAScene {
  loop() {
    console.log('loop');
  }
  keyboardEvent() {
    console.log('keyboardEvent');
  }
  mouseEvent() {
    console.log('mouseEvent');
  }
  tactilEvent() {
    console.log('tactilEvent');
  }
}

gameEngine.setScene(new MyScene(gameEngine));
```

---

## Engine

---

#### CatGameEngine

The CatGameEngine class is the one who's going to manage the whole application.

[Read more.](./CatGameEngine.md)

##### Example

```typescript
import { CatGameEngine } from '@msgame/catana';

const gameEngine = new CatGameEngine();

gameEngine.run();
```

---

## Drawable

---

### CatCircle

this class is a [ICatDrawableEntity](./CatGameEngine.md#icatdrawableentity).

It represents a cirlce that can be drawn by the [CatGameEngine](./CatGameEngine.md).

[Read more.](./CatCircle.md)

##### Example

```typescript
import { CatCircle } from '@msgame/catana';

const rect = new CatCircle({
  color: 'black',
  x: 0,
  y: 0,
  size: 100,
});
```

---

### CatImage

this class is a [ICatDrawableEntity](./CatGameEngine.md#icatdrawableentity).

It represents an image that can be drawn by the [CatGameEngine](./CatGameEngine.md).

[Read more.](./CatImage.md)

##### Example

```typescript
import { CatImage } from '@msgame/catana';

const image = new CatImage({
  src:
    'https://gitlab.com/marsattak-studio-game/catana/-/raw/master/src/assets/img/catana-logo.png',
});
```

---

### CatRect

this class is a [ICatDrawableEntity](./CatGameEngine.md#icatdrawableentity).

It represents a rect that can be drawn by the [CatGameEngine](./CatGameEngine.md).

[Read more.](./CatRect.md)

##### Example

```typescript
import { CatRect } from '@msgame/catana';

const rect = new CatRect({
  color: 'black',
  x: 0,
  y: 0,
  width: 100,
  height: 50,
});
```

---

### CatSprite extends [CatImage](#catimage)

this class is a [ICatDrawableEntity](./CatGameEngine.md#icatdrawableentity).

It represents a sprite that can be drawn by the [CatGameEngine](./CatGameEngine.md).

it can define the section of image to draw.

[Read more.](./CatImage.md)

##### Example

```typescript
import { CatSprite } from '@msgame/catana';

const sprite = new CatSprite({
  src:
    'https://gitlab.com/marsattak-studio-game/catana/-/raw/master/src/assets/img/catana-logo.png',
});
```

---

### CatText

this class is a [ICatDrawableEntity](./CatGameEngine.md#icatdrawableentity).

It represents a text that can be drawn by the [CatGameEngine](./CatGameEngine.md).

[Read more.](./CatText.md)

##### Example

```typescript
import { CatText } from '@msgame/catana';

const rectBlue = new CatText({
  color: 'black',
  x: 0,
  y: 0,
  text: 'Hello world',
});
```

---

## Audio

---

### CatAudioManager

This class allows to manage the different sounds.

[Read more.](./CatAudioManager.md)

##### Example

```typescript
import { CatAudioManager } from '@msgame/catana';

const audioMgr = new CatAudioManager();

audioMgr.load([
  {
    name: 'sound_1',
    src: './sound_1.mp3',

}].then(() => {
  audioMgr.playSound('sound_1');
});
```

---

## Constants

---

### FONT_TYPE

this constant is a basic list of CSS Web Safe Font Combinations.

| Name            | Default                           |
| --------------- | --------------------------------- |
| DEFAULT         | "Times New Roman", Times, serif   |
| ARIAL           | Arial, Helvetica, sans-serif      |
| TIMES_NEW_ROMAN | "Times New Roman", Times, serif   |
| COURIER_NEW     | "Courier New", Courier, monospace |

---

### FONT_SIZE

this constant is an Helper list of pixel size.

| Name | Default |
| ---- | ------- |
| XXS  | 20px    |
| XS   | 30px    |
| SM   | 40px    |
| MD   | 60px    |
| LG   | 80px    |

---

## Functions

---

#### isTouchDevice

This function returns true if a touch screen is detected on the device.

##### Example

```typescript
import { CatGameEngine, isTouchDevice } from '@msgame/catana';

const gameEngine = new CatGameEngine();

gameEngine.loop(() => {
  if (isTouchDevice()) {
    console.log('has a touch screen.');
  } else {
    console.log('does not have a touch screen.');
  }
});
```

---
