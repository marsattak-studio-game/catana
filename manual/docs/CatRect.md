 <div align="center">

# Catana - CatRect Documentation

![catane-logo](../../src/assets/img/catana-logo.png)

**[Go Home](../../README.md)**

</div>

---

## CatRect

this class is a [ICatDrawableEntity](./CatGameEngine.md#icatdrawableentity).

It represents a rect that can be drawn by the [CatGameEngine](./CatGameEngine.md).

### Methods

- [constructor](#constructor)
- [scale](#scale)
- [resetScale](#resetscale)
- [getColor](#getcolor)
- [setColor](#setcolor)
- [getAlpha](#getalpha)
- [setAlpha](#setalpha)
- [getX](#getx)
- [setX](#setx)
- [getY](#gety)
- [setY](#sety)
- [getWidth](#getwidth)
- [setWidth](#setwidth)
- [getHeight](#getheight)
- [setHeight](#setheight)
- [getRotation](#getrotation)
- [setRotation](#setrotation)

---

## constructor

`constructor(config: ICatRectConstructor)`

Create an instance of CatRect.

You can change the configuration with an [ICatRectConstructor](#icatrectconstructor) object.

### Example

```typescript
import { CatRect, ICatRectConstructor } from '@msgame/catana';

const config: ICatRectConstructor = {
  color: 'black',
  x: 50,
  y: 50,
  width: 150,
  height: 100,
};

const rect = new CatRect(config);
```

### ICatRectConstructor

This interface allows to define an object for the [CatRect constructor](#constructor).

| Name   | Description              | Type     | Required | Default |
| ------ | ------------------------ | -------- | -------- | ------- |
| color  | rect color               | `string` | yes      |         |
| x      | X position of the rect   | `number` | yes      |         |
| y      | Y position of the rect   | `number` | yes      |         |
| width  | Width of the rect        | `number` | yes      |         |
| height | Height of the rect       | `number` | yes      |         |
| alpha  | transparence of the rect | `number` | no       | 1       |

---

## scale

`scale(value: number): void`

Scale the rect.

### Example

```typescript
import { CatRect } from '@msgame/catana';

const rect = new CatRect({
  color: 'black',
  x: 50,
  y: 50,
  width: 150,
  height: 100,
});

rect.getWidth(); // 150
rect.scale(2);
rect.getWidth(); // 300
```

---

## resetScale

`resetScale(): void`

Reset the scale to the default value (reinit size).

### Example

```typescript
import { CatRect } from '@msgame/catana';

const rect = new CatRect({
  color: 'black',
  x: 50,
  y: 50,
  width: 150,
  height: 100,
});

rect.getWidth(); // 150
rect.scale(2);
rect.getWidth(); // 300
rect.resetScale();
rect.getWidth(); // 150
```

---

## getColor

`getColor(): string`

Return the color of the rect.

### Example

```typescript
import { CatRect } from '@msgame/catana';

const rect = new CatRect({
  color: 'black',
  x: 50,
  y: 50,
  width: 150,
  height: 100,
});

const colorOfTheRect = rect.getColor();
```

---

## setColor

`setColor(color: string): void`

Set the color of the rect.

### Example

```typescript
import { CatRect } from '@msgame/catana';

const rect = new CatRect({
  color: 'black',
  x: 50,
  y: 50,
  width: 150,
  height: 100,
});

rect.setColor('blue');
```

---

## getAlpha

`getAlpha(): number`

Return alpha of the rect.

### Example

```typescript
import { CatRect } from '@msgame/catana';

const rect = new CatRect({
  color: 'black',
  x: 50,
  y: 50,
  width: 150,
  height: 100,
});

const alphaOfTheRect = rect.getAlpha();
```

---

## setAlpha

`setAlpha(x: number): void`

Set alpha of the rect.

### Example

```typescript
import { CatRect } from '@msgame/catana';

const rect = new CatRect({
  color: 'black',
  x: 50,
  y: 50,
  width: 150,
  height: 100,
});

rect.setAlpha(0.5);
```

---

## getX

`getX(): number`

Return the X position of the rect on the screen.

### Example

```typescript
import { CatRect } from '@msgame/catana';

const rect = new CatRect({
  color: 'black',
  x: 50,
  y: 50,
  width: 150,
  height: 100,
});

const xPosOfTheRect = rect.getX();
```

---

## setX

`setX(x: number): void`

Set the X position of the rect on the screen.

### Example

```typescript
import { CatRect } from '@msgame/catana';

const rect = new CatRect({
  color: 'black',
  x: 50,
  y: 50,
  width: 150,
  height: 100,
});

rect.setX(100);
```

---

## getY

`getY(): number`

Return the Y position of the rect on the screen.

### Example

```typescript
import { CatRect } from '@msgame/catana';

const rect = new CatRect({
  color: 'black',
  x: 50,
  y: 50,
  width: 150,
  height: 100,
});

const yPosOfTheRect = rect.getY();
```

---

## setY

`setY(y: number): void`

Set the Y position of the rect on the screen.

### Example

```typescript
import { CatRect } from '@msgame/catana';

const rect = new CatRect({
  color: 'black',
  x: 50,
  y: 50,
  width: 150,
  height: 100,
});

rect.setY(100);
```

---

## getWidth

`getWidth(): number`

Return the Width of the rect.

### Example

```typescript
import { CatRect } from '@msgame/catana';

const rect = new CatRect({
  color: 'black',
  x: 50,
  y: 50,
  width: 150,
  height: 100,
});

const widthOfTheRect = rect.getWidth();
```

---

## setWidth

`setWidth(width: number): void`

Set the Width of the rect.

### Example

```typescript
import { CatRect } from '@msgame/catana';

const rect = new CatRect({
  color: 'black',
  x: 50,
  y: 50,
  width: 150,
  height: 100,
});

rect.setWidth(100);
```

---

## getHeight

`getHeight(): number`

Return the Height of the rect.

### Example

```typescript
import { CatRect } from '@msgame/catana';

const rect = new CatRect({
  color: 'black',
  x: 50,
  y: 50,
  width: 150,
  height: 100,
});

const heightOfTheRect = rect.getHeight();
```

---

## setHeight

`setHeight(height: number)`

Set the Height of the rect.

### Example

```typescript
import { CatRect } from '@msgame/catana';

const rect = new CatRect({
  color: 'black',
  x: 50,
  y: 50,
  width: 150,
  height: 100,
});

rect.setHeight(100);
```

---

## getRotation

`getRotation(): number`

Returns the rotation value of the rect.

> **The return value ranges from 0 to 359.**

### Example

```typescript
import { CatRect } from '@msgame/catana';

const rect = new CatRect({
  color: 'black',
  x: 50,
  y: 50,
  width: 150,
  height: 100,
});

const rotationValueOfTheRect = rect.getRotation();
```

---

## setRotation

`setRotation(rotation: number)`

Set the rotation value of the rect.

### Example

```typescript
import { CatRect } from '@msgame/catana';

const rect = new CatRect({
  color: 'black',
  x: 50,
  y: 50,
  width: 150,
  height: 100,
});

rect.setRotation(45);
```

---
