const path = require('path');

module.exports = (env, args) => {
  const mode = args.mode || 'development';
  return {
    entry: './src',
    mode,
    plugins: [],
    resolve: {
      extensions: ['.ts', '.js'],
    },
    module: {
      rules: [
        {
          test: /\.ts(x?)$/,
          exclude: /node_modules/,
          use: [
            'babel-loader',
            {
              loader: 'ts-loader',
            },
          ],
        },
        {
          test: /\.js$/,
          loader: 'babel-loader',
          exclude: /node_modules/,
        },
        {
          test: /\.(ico|jpe?g|png|gif)$/,
          loaders: [
            {
              loader: 'url-loader',
            },
          ],
        },
        {
          test: /\.css$/,
          loaders: [
            {
              loader: 'to-string-loader',
            },
            {
              loader: 'css-loader',
            },
          ],
        },
      ],
    },
    output: {
      libraryTarget: 'umd',
      library: {
        root: 'Catana',
        commonjs: '@msgame/catana',
        amd: '@msgame/catana',
      },
      path: path.resolve(__dirname, 'dist'),
      filename: 'index.js',
    },
  };
};
