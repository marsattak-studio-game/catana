class MyScene extends Catana.CatAScene {
  constructor(ge) {
    super(ge);
  }

  loop() {
    console.log('loop');
  }

  mouseEvent() {
    console.log('mouseEvent');
  }
  keyboardEvent() {
    console.log('keyboardEvent');
  }
  mouseEvent() {
    console.log('mouseEvent');
  }
}

window.addEventListener('load', async () => {
  const gameEngine = new Catana.CatGameEngine();
  const audioManager = new Catana.CatAudioManager();
  try {
    await audioManager.load([
      {
        name: 'ui_click',
        src: './ui_click.ogg',
      },
    ]);
    const circle = new Catana.CatCircle({
      x: 0,
      y: 0,
      color: 'red',
      size: 100,
    });
    audioManager.playSound('ui_click');
    gameEngine.setScene(new MyScene(gameEngine));
    gameEngine.loop(({ draw }) => {
      draw(circle);
    });

    gameEngine.run();
  } catch (e) {
    // eslint-disable-next-line no-console
    console.error(e);
  }
});
