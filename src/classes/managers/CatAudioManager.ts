import { Logger } from '../Logger';

interface ICatAudioManagerAudioToLoad<ISoundName extends string> {
  name: ISoundName;
  src: string;
}

type ISoundLoaded<ISoundName extends string> = {
  [data in ISoundName]: HTMLAudioElement;
};

export class CatAudioManager<ISoundName extends string> {
  private _soundsLoaded: ISoundLoaded<ISoundName> | null = null;

  /**
   * Allows you to load sounds.

   * @param {ICatAudioManagerAudioToLoad<ISoundName>[]} audioSources - List of sounds to load.
   * @returns {Promise<void>} - Returns a resolved promise once all sounds are loaded or reject if there was an error.
   *
   * @example
   * type IMySoundName = 'sound_1' | 'sound_2';
   *
   * const audioSources: ICatAudioManagerAudioToLoad<ISoundName>[] = [
   *  {
   *    name: 'sound_1',
   *    src: './sound_1.mp3',
   *  },
   *  {
   *    name: 'sound_2',
   *    src: './sound_2.mp3',
   *  }
   * ];
   *
   * const audioMgr = new CatAudioManager<IMySoundName>();
   *
   * audioMgr.load(audioSources).then(() => {
   *  // Sounds loaded
   * });
   */
  public load(
    audioSources: ICatAudioManagerAudioToLoad<ISoundName>[],
  ): Promise<void> {
    return new Promise((resolve, reject) => {
      // If list is not empty, try to load
      if (audioSources.length > 0) {
        const audioSourcesLength = audioSources.length;
        let audioLoadedCounter = 0;

        // Should to check is all sounds is loaded
        const checkIfSoundsIsLoaded = (): void => {
          setTimeout(() => {
            if (audioLoadedCounter < audioSourcesLength) {
              checkIfSoundsIsLoaded();
            } else {
              resolve();
            }
          }, 500);
        };

        // Load audio sources
        audioSources.map(audioSource => {
          this._soundsLoaded = {
            ...this._soundsLoaded,
            [audioSource.name]: new Audio(audioSource.src),
          } as ISoundLoaded<ISoundName>;
          this._soundsLoaded[audioSource.name].load();
          this._soundsLoaded[audioSource.name].addEventListener(
            'canplaythrough',
            (): void => {
              audioLoadedCounter++;
            },
          );
          this._soundsLoaded[audioSource.name].addEventListener(
            'error',
            (e): void => {
              reject(e);
            },
          );
        });

        checkIfSoundsIsLoaded();
      } else {
        reject('[CatAudioManagerError] - audioSources list is empty.');
      }
    });
  }

  /**
   * Allows you to play a sound.
   * @param {ISoundName} name - The name of the sound.
   */
  public playSound(name: ISoundName): void {
    if (this._soundsLoaded) {
      const sound = this._soundsLoaded[name] as HTMLAudioElement;
      if (sound) {
        sound.pause();
        sound.currentTime = 0;
        sound.play();
      } else {
        Logger.error(
          `[CatAudioManagerError] - playSound: ${name} sound does not exist`,
        );
      }
    } else {
      Logger.error(
        `[CatAudioManagerError] - audio list is not loaded: please use the "CatAudioManager.load(audioSources)"`,
      );
    }
  }

  /**
   * Allows to stop a sound.
   * @param {ISoundName} name - The name of the sound to be stopped.
   * @param {number} [smooth=0] - Allows you to gradually lower the volume before turning off the sound.
   * @returns {Promise<void>} - Returns a resolved promise once the sound is finished or reject if there was an error.
   */
  public async stopSound(name: ISoundName, smooth = 0): Promise<void> {
    return new Promise((resolve, reject) => {
      if (this._soundsLoaded) {
        const sound = this._soundsLoaded[name] as HTMLAudioElement;
        if (sound) {
          const startTime = Date.now();
          const startVolume = sound.volume;
          function smoothStop(): void {
            setTimeout(() => {
              const diffTime = smooth + startTime - Date.now();
              if (diffTime >= 0) {
                const percent = (diffTime * 100) / smooth;
                const newVolume = (startVolume / 100) * percent;
                sound.volume = newVolume;
                smoothStop();
              } else {
                sound.pause();
                sound.currentTime = 0;
                resolve();
              }
            });
          }
          smoothStop();
        } else {
          reject('[CatAudioManagerError] - Can not stop an undefined sound');
        }
      } else {
        reject(
          `[CatAudioManagerError] - audio list is not loaded: please use the "CatAudioManager.load(audioSources)"`,
        );
      }
    });
  }
}
