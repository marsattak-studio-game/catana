import { Debug } from '../utils/development/Debug';

/* eslint-disable no-console */
export class Logger {
  /** ---------------------
   *          LOG
   * ---------------------- */

  /**
   * Allows you to display a message in the console.
   * @param {...unknown[]} values
   */
  public static log(...values: unknown[]): void {
    console.log(...values);
  }

  /**
   * Allows you to display a message in the console only in development mode.
   * @param {...unknown[]} values
   */
  public static logDev(...values: unknown[]): void {
    Debug.isActivate() && console.log(...values);
  }

  /**
   * Allows you to display a message in the console only in production mode.
   * @param {...unknown[]} values
   */
  public static logProd(...values: unknown[]): void {
    !Debug.isActivate() && console.log(...values);
  }

  /** ---------------------
   *          WARN
   * ---------------------- */

  /**
   * Allows you to display a warning in the console.
   * @param {...unknown[]} values
   */
  public static warn(...values: unknown[]): void {
    console.warn(...values);
  }

  /**
   * Allows you to display a warning in the console only in development mode.
   * @param {...unknown[]} values
   */
  public static warnDev(...values: unknown[]): void {
    Debug.isActivate() && console.warn(...values);
  }

  /**
   * Allows you to display a warning in the console only in production mode.
   * @param {...unknown[]} values
   */
  public static warnProd(...values: unknown[]): void {
    !Debug.isActivate() && console.warn(...values);
  }

  /** ---------------------
   *          ERROR
   * ---------------------- */

  /**
   * Allows you to display a error in the console.
   * @param {...unknown[]} values
   */
  public static error(...values: unknown[]): void {
    console.error(...values);
  }
  /**
   * Allows you to display a error in the console only in development mode.
   * @param {...unknown[]} values
   */
  public static errorDev(...values: unknown[]): void {
    Debug.isActivate() && console.error(...values);
  }
  /**
   * Allows you to display a error in the console only in production mode.
   * @param {...unknown[]} values
   */
  public static errorProd(...values: unknown[]): void {
    !Debug.isActivate() && console.error(...values);
  }

  /** ---------------------
   *          DIR
   * ---------------------- */

  /**
   * Allows you to display a list of object properties in the console.
   * @param {unknown} values
   */
  public static dir(values: unknown): void {
    console.dir(values);
  }
  /**
   * Allows you to display a list of object properties in the console only in development mode.
   * @param {unknown} values
   */
  public static dirDev(values: unknown): void {
    Debug.isActivate() && console.dir(values);
  }
  /**
   * Allows you to display a list of object properties in the console only in production mode.
   * @param {unknown} values
   */
  public static dirProd(values: unknown): void {
    !Debug.isActivate() && console.dir(values);
  }

  /** ---------------------
   *          TABLE
   * ---------------------- */

  /**
   * Allows you to display a table of the object in the console.
   * @param {unknown} values
   */
  public static table(values: unknown): void {
    console.table(values);
  }
  /**
   * Allows you to display a table of the object in the console only in development mode.
   * @param {unknown} values
   */
  public static tableDev(values: unknown): void {
    Debug.isActivate() && console.table(values);
  }
  /**
   * Allows you to display a table of the object in the console only in production mode.
   * @param {unknown} values
   */
  public static tableProd(values: unknown): void {
    !Debug.isActivate() && console.dir(values);
  }
}
