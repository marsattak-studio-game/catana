import { CatGameEngine, ICatGameEngineLoopAgs } from '../CatGameEngine';

export type IKeyboardEventType = 'keyup' | 'keydown' | 'keydown';
export type IMouseEventType = 'mouseup' | 'mousedown' | 'mousemove';
export type ITouchEventType = 'touchend' | 'touchstart' | 'touchmove';

export abstract class CatAScene {
  private _gameEngine: CatGameEngine;

  constructor(ge: CatGameEngine) {
    this._gameEngine = ge;
  }

  protected getGameEngine(): CatGameEngine {
    return this._gameEngine;
  }

  public abstract loop(args: ICatGameEngineLoopAgs): void;

  public abstract keyboardEvent(
    event: KeyboardEvent,
    type: IKeyboardEventType,
  ): void;
  public abstract mouseEvent(event: MouseEvent, type: IMouseEventType): void;
  public abstract tactilEvent(event: TouchEvent, type: ITouchEventType): void;
}
