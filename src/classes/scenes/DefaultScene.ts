import * as imgTest from '../../assets/img/catana-logo.png';

import { CatGameEngine, ICatGameEngineLoopAgs } from '../CatGameEngine';

import { CatAScene } from './CatAScene';
import { CatImage } from '../drawable/CatImage';
import { CatText } from '../drawable/CatText';
import { FONT_SIZE } from '../../config/texts';

export class DefaultScene extends CatAScene {
  constructor(ge: CatGameEngine) {
    super(ge);
  }

  public loop({ draw }: ICatGameEngineLoopAgs): void {
    const image = new CatImage({ src: imgTest });
    image.setX(this.getGameEngine().getScreenWidth() / 2 - image.width / 2);
    image.setY(this.getGameEngine().getScreenHeight() / 2 - image.height / 2);
    draw(image);
    const text = new CatText({
      text: 'Catana project',
      x: 40,
      y: this.getGameEngine().getScreenHeight() - 120,
      color: 'black',
      size: FONT_SIZE.MD,
    });
    draw(text);
  }

  // eslint-disable-next-line @typescript-eslint/no-empty-function
  public keyboardEvent(): void {}

  // eslint-disable-next-line @typescript-eslint/no-empty-function
  public mouseEvent(): void {}

  // eslint-disable-next-line @typescript-eslint/no-empty-function
  public tactilEvent(): void {}
}
