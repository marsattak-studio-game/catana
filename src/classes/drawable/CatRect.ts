interface ICatRectConstructor {
  color: string;
  x: number;
  y: number;
  width: number;
  height: number;
  alpha?: number;
}

export class CatRect {
  private _color: string;
  private _defaultWidth: number;
  private _defaultHeight: number;
  private _width: number;
  private _height: number;
  private _pos: { x: number; y: number };
  private _alpha: number;
  private _rotation = 0;

  constructor({ color, x, y, width, height, alpha = 1 }: ICatRectConstructor) {
    this._color = color;
    this._pos = {
      x,
      y,
    };
    this._width = width;
    this._height = height;
    this._defaultWidth = width;
    this._defaultHeight = height;
    this._alpha = alpha;
  }

  public scale(value: number): void {
    const lastWidth = this._width;
    const lastHeight = this._height;
    this._width = lastWidth * value;
    this._height = lastHeight * value;
  }

  public resetScale(): void {
    this._width = this._defaultWidth;
    this._height = this._defaultHeight;
  }

  public getColor(): string {
    return this._color;
  }

  public setColor(color: string): void {
    this._color = color;
  }

  public getAlpha(): number {
    return this._alpha;
  }

  public setAlpha(alpha: number): void {
    this._alpha = alpha;
  }

  public getX(): number {
    return this._pos.x;
  }

  public setX(x: number): void {
    this._pos.x = x;
  }

  public getY(): number {
    return this._pos.y;
  }

  public setY(y: number): void {
    this._pos.y = y;
  }

  public getRotation(): number {
    return this._rotation;
  }

  public setRotation(rotation: number): void {
    this._rotation = rotation % 360;
  }

  public getWidth(): number {
    return this._width;
  }

  public setWidth(width: number): void {
    this._width = width;
  }

  public getHeight(): number {
    return this._height;
  }

  public setHeight(height: number): void {
    this._height = height;
  }
}
