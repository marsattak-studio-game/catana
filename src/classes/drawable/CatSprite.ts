import { CatImage, ICatImageConstructor } from './CatImage';
interface ICatSpriteConstructor extends ICatImageConstructor {
  xSection?: number;
  ySection?: number;
  widthSection?: number;
  heightSection?: number;
}

interface ICatSpriteSection {
  x: number;
  y: number;
  width: number;
  height: number;
}

export class CatSprite extends CatImage {
  private _section: ICatSpriteSection;

  constructor({
    xSection,
    ySection,
    widthSection,
    heightSection,
    ...catImageArgs
  }: ICatSpriteConstructor) {
    super(catImageArgs);
    this._section = {
      x: xSection || 0,
      y: ySection || 0,
      width: widthSection || this.width,
      height: heightSection || this.height,
    };

    this.addEventListener('load', () => {
      this._section.width = widthSection || this.width;
      this._section.height = heightSection || this.height;
    });
  }

  public getSpriteSection(): ICatSpriteSection {
    return this._section;
  }

  public setSpriteSection(section: ICatSpriteSection): void {
    this._section = section;
  }
}
