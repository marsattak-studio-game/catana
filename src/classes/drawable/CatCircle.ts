interface ICatCircleConstructor {
  color: string;
  x: number;
  y: number;
  size: number;
}

export class CatCircle {
  private _color: string;
  private _defaultSize: number;
  private _size: number;
  private _pos: { x: number; y: number };
  private _rotation = 0;

  constructor({ color, x, y, size }: ICatCircleConstructor) {
    this._color = color;
    this._pos = {
      x,
      y,
    };
    this._size = size;
    this._defaultSize = size;
  }

  public scale(value: number): void {
    const lastSize = this._size;
    this._size = lastSize * value;
  }

  public resetScale(): void {
    this._size = this._defaultSize;
  }

  public getColor(): string {
    return this._color;
  }

  public setColor(color: string): void {
    this._color = color;
  }

  public getX(): number {
    return this._pos.x;
  }

  public setX(x: number): void {
    this._pos.x = x;
  }

  public getY(): number {
    return this._pos.y;
  }

  public setY(y: number): void {
    this._pos.y = y;
  }

  public getRotation(): number {
    return this._rotation;
  }

  public setRotation(rotation: number): void {
    this._rotation = rotation % 360;
  }

  public getSize(): number {
    return this._size;
  }

  public setSize(size: number): void {
    this._size = size;
  }
}
