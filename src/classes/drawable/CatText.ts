import { FONT_SIZE, FONT_TYPE } from '../../config/texts';
interface ITextShadow {
  offsetXShadow: number;
  offsetYShadow: number;
  colorShadow: string;
}

interface ICatTextConstructor {
  text: string;
  x: number;
  y: number;
  color: string;
  size?: string;
  alpha?: number;
  offsetXShadow?: number;
  offsetYShadow?: number;
  colorShadow?: string;
  font?: string;
}

export class CatText {
  private _text: string;
  private _color: string;
  private _pos: { x: number; y: number };
  private _alpha: number;
  private _font: string;
  private _size: string;
  private _shadow: ITextShadow;
  private _rotation = 0;

  constructor({
    text,
    color,
    x,
    y,
    font = FONT_TYPE.DEFAULT,
    size = FONT_SIZE.SM,
    offsetXShadow = 0,
    offsetYShadow = 0,
    colorShadow = 'black',
    alpha = 1,
  }: ICatTextConstructor) {
    this._text = text;
    this._color = color;
    this._pos = {
      x,
      y,
    };
    this._font = font;
    this._size = size;
    this._shadow = {
      offsetXShadow,
      offsetYShadow,
      colorShadow,
    };
    this._alpha = alpha;
  }

  public getColor(): string {
    return this._color;
  }

  public setColor(color: string): void {
    this._color = color;
  }

  public getAlpha(): number {
    return this._alpha;
  }

  public setAlpha(alpha: number): void {
    this._alpha = alpha;
  }

  public getX(): number {
    return this._pos.x;
  }

  public setX(x: number): void {
    this._pos.x = x;
  }

  public getY(): number {
    return this._pos.y;
  }

  public setY(y: number): void {
    this._pos.y = y;
  }

  public getRotation(): number {
    return this._rotation;
  }

  public setRotation(rotation: number): void {
    this._rotation = rotation % 360;
  }

  public getText(): string {
    return this._text;
  }

  public setText(text: string): void {
    this._text = text;
  }

  public getFont(): string {
    return this._font;
  }

  public setFont(font: string): void {
    this._font = font;
  }

  public getSize(): string {
    return this._size;
  }

  public setSize(size: string): void {
    this._size = size;
  }

  public getShadow(): ITextShadow {
    return this._shadow;
  }

  public setShadow(shadow: ITextShadow): void {
    this._shadow = shadow;
  }
}
