export interface ICatImageConstructor {
  src: string;
  x?: number;
  y?: number;
  width?: number;
  height?: number;
}

export class CatImage extends Image {
  private _isLoaded = false;
  private _defaultWidth = 0;
  private _defaultHeight = 0;
  private _width = 0;
  private _height = 0;
  private _pos: { x: number; y: number };
  private _rotation = 0;

  constructor({ src, x = 0, y = 0, width, height }: ICatImageConstructor) {
    super();
    this.src = src;
    this._pos = {
      x,
      y,
    };
    this._width = width || this.width;
    this._height = height || this.height;
    this._defaultWidth = this.width;
    this._defaultHeight = this.height;
    this.addEventListener('load', () => {
      this._isLoaded = true;
      this._width = width || this.width;
      this._height = height || this.height;
      this._defaultWidth = this.width;
      this._defaultHeight = this.height;
    });
  }

  public scale(value: number): void {
    if (this._isLoaded) {
      const lastWidth = this._width;
      const lastHeight = this._height;
      this._width = lastWidth * value;
      this._height = lastHeight * value;
    } else {
      this.addEventListener('load', () => {
        this.scale(value);
      });
    }
  }

  public resetScale(): void {
    this._width = this._defaultWidth;
    this._height = this._defaultHeight;
  }

  public getX(): number {
    return this._pos.x;
  }

  public setX(x: number): void {
    this._pos.x = x;
  }

  public getY(): number {
    return this._pos.y;
  }

  public setY(y: number): void {
    this._pos.y = y;
  }

  public getRotation(): number {
    return this._rotation;
  }

  public setRotation(rotation: number): void {
    this._rotation = rotation % 360;
  }

  public getWidth(): number {
    return this._width;
  }

  public setWidth(width: number): void {
    this._width = width;
  }

  public getHeight(): number {
    return this._height;
  }

  public setHeight(height: number): void {
    this._height = height;
  }
}
