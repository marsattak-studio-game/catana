import * as style from '../assets/css/style.css';

import {
  CatAScene,
  IKeyboardEventType,
  IMouseEventType,
  ITouchEventType,
} from './scenes/CatAScene';
import {
  drawCircle,
  drawImage,
  drawRect,
  drawSprite,
  drawText,
  initializeCatGameEngine,
  measureText,
} from './utils';

import { CatCircle } from './drawable/CatCircle';
import { CatImage } from './drawable/CatImage';
import { CatRect } from './drawable/CatRect';
import { CatSprite } from './drawable/CatSprite';
import { CatText } from './drawable/CatText';
import { Debug } from '../utils/development/Debug';
import { DefaultScene } from './scenes/DefaultScene';
import { isTouchDevice } from '../utils/screen/isTouchDevice';

export type ICatDrawableEntity =
  | CatImage
  | CatRect
  | CatText
  | CatSprite
  | CatCircle;

export interface ICatGameEngineConstructor {
  debugMode?: boolean;
  containerId?: string;
  initialScreenWidth?: number;
  initialScreenHeight?: number;
}

export interface ICatGameEngineLoopAgs {
  draw: (entity: ICatDrawableEntity) => void;
  measureText: (text: CatText) => TextMetrics;
}

const DEFAULT_SCREEN_CONTAINER_ID = 'catana-screen';
const DEFAULT_FULLSCREEN_CONTAINER_ID = 'catana-fullscreen';

/**
 * This is the CatGameEngine class.
 * She's the one who's going to manage the whole application
 */
export class CatGameEngine {
  private _debugMode: boolean;
  private _containerId: string;
  private _canvasId: string;
  private _fullscreenId: string;
  private _consoleElt: HTMLElement;
  private _canvasElt: HTMLCanvasElement;
  private _contextElt: CanvasRenderingContext2D;
  private _initialScreenWidth: number;
  private _initialScreenHeight: number;
  private _scene: CatAScene;

  /**
   * Create an instance of CatGameEngine.
   * @param {ICatGameEngineConstructor} config The constructor configuration.
   */
  constructor({
    containerId,
    debugMode,
    initialScreenWidth = 1280,
    initialScreenHeight = 768,
  }: ICatGameEngineConstructor = {}) {
    this._debugMode = !!debugMode;
    this._containerId = !containerId
      ? DEFAULT_SCREEN_CONTAINER_ID
      : containerId;
    this._initialScreenWidth = initialScreenWidth;
    this._initialScreenHeight = initialScreenHeight;
    this._canvasId = `${this._containerId}-canvas`;
    this._fullscreenId = DEFAULT_FULLSCREEN_CONTAINER_ID;

    initializeCatGameEngine(
      this._containerId,
      this._canvasId,
      this._fullscreenId,
      !!this._containerId,
      this._initialScreenWidth,
      this._initialScreenHeight,
    );

    this._consoleElt = document.getElementById(
      this._containerId,
    ) as HTMLElement;
    this._canvasElt = document.getElementById(
      this._canvasId,
    ) as HTMLCanvasElement;
    this._contextElt = this._canvasElt.getContext(
      '2d',
    ) as CanvasRenderingContext2D;

    this._scene = new DefaultScene(this);

    this._initEvents();
    debugMode && Debug.init();
  }

  /**
   * This function allows to define the function of the main infiny loop.
   * @param callback - function to be added to the engine
   */
  public loop(callback: (args: ICatGameEngineLoopAgs) => void): void {
    this._scene.loop = callback;
  }

  /**
   * This function allows you to launch the application
   */
  public run(): void {
    this._load();
    this._mainLoop();
  }

  /**
   * This function allows you to capture keyboard events.
   * @param callback - function to get event
   */
  public keyboardEvent(
    callback: (event: KeyboardEvent, type: IKeyboardEventType) => void,
  ): void {
    this._scene.keyboardEvent = callback;
  }

  /**
   * This function allows to define the function for get mouse event.
   * @param callback - function to get event
   */
  public mouseEvent(
    callback: (event: MouseEvent, type: IMouseEventType) => void,
  ): void {
    this._scene.mouseEvent = callback;
  }

  /**
   * This function allows to define the function for get tactil event.
   * @param callback - function to get event
   */
  public tactilEvent(
    callback: (event: TouchEvent, type: ITouchEventType) => void,
  ): void {
    this._scene.tactilEvent = callback;
  }

  /**
   * This function allows to define the scene.
   * @param scene - the new scene.
   */
  public setScene(scene: CatAScene): void {
    this._scene = scene;
  }

  public getInitialScreenWidth(): number {
    return this._initialScreenWidth;
  }

  public getInitialScreenHeight(): number {
    return this._initialScreenHeight;
  }

  public getScreenWidth(): number {
    return this._canvasElt.width;
  }

  public getScreenHeight(): number {
    return this._canvasElt.height;
  }

  public scaleScreen(scaleX: number, scaleY?: number): void {
    this._contextElt.scale(scaleX, scaleY || scaleX);
  }

  // ********************************************
  // **************** PRIVATE *******************
  // ********************************************

  private _draw(entity: ICatDrawableEntity): void {
    // INFO: Get rotation center of the entity
    let entityWidth;
    let entityHeight;
    if (entity instanceof CatText) {
      entityWidth = this._measureText(entity).width;
      entityHeight = parseInt(entity.getSize().split(/\D/)[0], 10);
    } else if (entity instanceof CatCircle) {
      entityWidth = entity.getSize();
      entityHeight = entity.getSize();
    } else {
      entityWidth = entity.getWidth();
      entityHeight = entity.getHeight();
    }
    const xCentre = entity.getX() + entityWidth / 2;
    const yCentre = entity.getY() + entityHeight / 2;
    this._contextElt.save();
    this._contextElt.translate(xCentre, yCentre);

    // Rotate
    this._contextElt.rotate((Math.PI / 180) * entity.getRotation());
    this._contextElt.translate(-xCentre, -yCentre);

    // Draw
    if (entity instanceof CatSprite) {
      drawSprite(this._contextElt, entity);
    } else if (entity instanceof CatImage) {
      drawImage(this._contextElt, entity);
    } else if (entity instanceof CatRect) {
      drawRect(this._contextElt, entity);
    } else if (entity instanceof CatText) {
      drawText(this._contextElt, entity);
    } else if (entity instanceof CatCircle) {
      drawCircle(this._contextElt, entity);
    }
    this._contextElt.restore();
  }

  private _measureText(text: CatText): TextMetrics {
    return measureText(this._contextElt, text);
  }

  private _load(): void {
    // Apply style
    const appStyleElt = window.document.createElement('style');
    appStyleElt.type = 'text/css';
    appStyleElt.innerText = style;
    window.document.head.appendChild(appStyleElt);
    this._contextElt.textBaseline = 'top';
  }

  private _mainLoop(): void {
    const loopArgs: ICatGameEngineLoopAgs = {
      draw: (entity: ICatDrawableEntity) => this._draw(entity),
      measureText: (text: CatText) => this._measureText(text),
    };
    this._contextElt.save();
    this._contextElt.clearRect(
      0,
      0,
      this._canvasElt.width,
      this._canvasElt.height,
    );
    this._scene.loop(loopArgs);
    Debug.addFps();
    this._contextElt.restore();
    window.requestAnimFrame &&
      window.requestAnimFrame(() => {
        this._mainLoop();
      });
  }

  private _initEvents(): void {
    if (isTouchDevice()) {
      this._canvasElt.addEventListener('touchend', (e: TouchEvent) =>
        this._scene.tactilEvent(e, 'touchend'),
      );

      this._canvasElt.addEventListener('touchmove', (e: TouchEvent) =>
        this._scene.tactilEvent(e, 'touchmove'),
      );

      this._canvasElt.addEventListener('touchstart', (e: TouchEvent) =>
        this._scene.tactilEvent(e, 'touchstart'),
      );
    } else {
      this._canvasElt.addEventListener('mousemove', (e: MouseEvent) =>
        this._scene.mouseEvent(e, 'mousemove'),
      );

      this._canvasElt.addEventListener('mousedown', (e: MouseEvent) =>
        this._scene.mouseEvent(e, 'mousedown'),
      );

      this._canvasElt.addEventListener('mouseup', (e: MouseEvent) =>
        this._scene.mouseEvent(e, 'mouseup'),
      );
      window?.addEventListener('keydown', (e: KeyboardEvent) =>
        this._scene.keyboardEvent(e, 'keydown'),
      );

      window?.addEventListener('keyup', (e: KeyboardEvent) =>
        this._scene.keyboardEvent(e, 'keyup'),
      );
    }
  }
}
