import {
  generateScreen,
  initializeOnScreenResizeEvents,
  maximizeScreen,
} from '../utils/screen/functions';

import { CatCircle } from './drawable/CatCircle';
import { CatImage } from './drawable/CatImage';
import { CatRect } from './drawable/CatRect';
import { CatSprite } from './drawable/CatSprite';
import { CatText } from './drawable/CatText';
import { initializePolyfills } from '../utils/polyfills';

export function initializeCatGameEngine(
  containerId: string,
  canvasId: string,
  fullscreenId: string,
  createContainer = false,
  initialWidth: number,
  initialHeight: number,
): void {
  /* ******************* GLOBAL FUNCTION *********************** */
  initializePolyfills();
  generateScreen(
    containerId,
    canvasId,
    fullscreenId,
    createContainer,
    initialWidth,
    initialHeight,
  );
  maximizeScreen(containerId, canvasId);
  initializeOnScreenResizeEvents(containerId, canvasId, fullscreenId);
}

export function drawImage(
  context: CanvasRenderingContext2D,
  image: CatImage,
): void {
  context.drawImage(
    image,
    image.getX(),
    image.getY(),
    image.getWidth(),
    image.getHeight(),
  );
}

export function drawSprite(
  context: CanvasRenderingContext2D,
  sprite: CatSprite,
): void {
  context.drawImage(
    sprite,
    sprite.getSpriteSection().x,
    sprite.getSpriteSection().y,
    sprite.getSpriteSection().width,
    sprite.getSpriteSection().height,
    sprite.getX(),
    sprite.getY(),
    sprite.getWidth(),
    sprite.getHeight(),
  );
}

export function drawRect(
  context: CanvasRenderingContext2D,
  rect: CatRect,
): void {
  context.globalAlpha = rect.getAlpha();
  context.fillStyle = rect.getColor();
  context.fillRect(rect.getX(), rect.getY(), rect.getWidth(), rect.getHeight());
}

export function drawCircle(
  context: CanvasRenderingContext2D,
  circle: CatCircle,
): void {
  context.beginPath();
  context.arc(
    circle.getX() + circle.getSize(),
    circle.getY() + circle.getSize(),
    circle.getSize(),
    0,
    2 * Math.PI,
  );
  context.fill();
}

export function drawText(
  context: CanvasRenderingContext2D,
  text: CatText,
): void {
  context.shadowColor = text.getShadow().colorShadow;
  context.shadowOffsetX = text.getShadow().offsetXShadow;
  context.shadowOffsetY = text.getShadow().offsetYShadow;
  context.globalAlpha = text.getAlpha();
  context.fillStyle = text.getColor();
  context.font = `${text.getSize()} ${text.getFont()}`;
  context.fillText(text.getText(), text.getX(), text.getY());
}

export function measureText(
  context: CanvasRenderingContext2D,
  text: CatText,
): TextMetrics {
  context.font = `${text.getSize()} ${text.getFont()}`;
  return context.measureText(text.getText());
}
