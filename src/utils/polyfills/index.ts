/**
 * Polyfill for requestAnimationFrame function.
 */
function requestAnimFramePolyfill(): void {
  if (!window.requestAnimFrame) {
    window.requestAnimFrame = ((): ((
      callback: FrameRequestCallback,
    ) => number) =>
      window.requestAnimationFrame ||
      window.webkitRequestAnimationFrame ||
      window.mozRequestAnimationFrame ||
      window.oRequestAnimationFrame ||
      window.msRequestAnimationFrame ||
      function timeout(callback: FrameRequestCallback): number {
        return window.setTimeout(callback, 1000 / 60);
      })();
  }
}

/**
 * polyfill for Date.now() function.
 */
function datePolyfill(): void {
  if (!Date.now) Date.now = (): number => new Date().getTime();
}

/**
 * Initialize the polyfills for:
 * - Date.now()
 * - window.requestAnimationFrame
 */
export function initializePolyfills(): void {
  requestAnimFramePolyfill();
  datePolyfill();
}
