function createDebugView(): HTMLDivElement {
  const element = document.createElement('div');
  element.style.position = 'absolute';
  element.style.top = '0';
  element.style.left = '0';
  element.style.backgroundColor = 'white';
  element.style.color = 'black';
  element.style.border = '2px solid black';
  element.style.padding = '10px';
  element.style.fontWeight = 'bold';
  element.style.textTransform = 'uppercase';
  element.style.opacity = '0.85';
  element.style.display = 'none';
  element.innerText = 'Fps: 0';
  return element;
}

export class Debug {
  private static _debugModeIsActive = false;
  private static _fps = 0;
  private static _debugViewElement = createDebugView();

  public static init(): void {
    this.activeDebugMode(true);
    document.body.appendChild(Debug._debugViewElement);
    setInterval(() => {
      if (Debug._debugModeIsActive) {
        Debug._debugViewElement.innerText = `Fps: ${Debug._fps}`;
        Debug._fps = 0;
      }
    }, 1000);
  }

  public static addFps(): void {
    if (Debug._debugModeIsActive) {
      Debug._fps++;
    }
  }

  public static activeDebugMode(active: boolean): void {
    Debug._debugModeIsActive = active;
    Debug._debugViewElement.style.display = active ? 'block' : 'none';
  }

  public static isActivate(): boolean {
    return Debug._debugModeIsActive;
  }
}
