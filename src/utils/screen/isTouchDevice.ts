export function isTouchDevice(): boolean {
  return window && ('ontouchstart' in window || !!navigator.msMaxTouchPoints);
}
