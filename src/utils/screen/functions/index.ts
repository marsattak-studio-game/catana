/**
 * Allows you to maximize the size of the canvas in relation to the size of the screen container.
 *
 * @param {string} containerId - The id of the conatiner HTML element.
 * @param {string} canvasId - The id of the canvas HTML element.
 */
export function maximizeScreen(containerId: string, canvasId: string): void {
  const canvasElt = document.getElementById(canvasId) as HTMLCanvasElement;
  const initialWidth = canvasElt.width;
  const initialHeight = canvasElt.height;
  if (
    document.body.clientWidth * (initialHeight / initialWidth) <
    document.body.clientHeight
  ) {
    canvasElt.style.width = '100%';
    canvasElt.style.height = 'auto';
  } else {
    canvasElt.style.height = '100%';
    canvasElt.style.width = 'auto';
  }
}

/**
 * Initializes the resizing of the canvas during a container resizing event.
 *
 * @param {string} containerId - The id of the conatiner HTML element.
 * @param {string} canvasId - The id of the canvas HTML element.
 * @param {string} fullscreenId - The id of the fullscreen switcher HTML element.
 */
export function initializeOnScreenResizeEvents(
  containerId: string,
  canvasId: string,
  fullscreenId: string,
): void {
  const consoleElt = document.getElementById(containerId) as HTMLElement;
  const fullscreenElt = document.getElementById(fullscreenId) as HTMLElement;

  // ********** Resize **********
  window.addEventListener('resize', () =>
    maximizeScreen(containerId, canvasId),
  );

  // ********** FullScreen **********
  fullscreenElt.addEventListener(
    'touchend',
    e => {
      e.preventDefault();
      if (
        !document.fullscreenElement &&
        !document.mozFullScreenElement &&
        !document.webkitFullscreenElement
      ) {
        if (consoleElt.requestFullscreen) {
          consoleElt.requestFullscreen();
        } else if (consoleElt.mozRequestFullscreen) {
          consoleElt.mozRequestFullscreen();
        } else if (consoleElt.webkitRequestFullscreen) {
          consoleElt.webkitRequestFullscreen();
        }
      } else if (document.cancelFullScreen) {
        document.cancelFullScreen();
      } else if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
      } else if (document.webkitCancelFullScreen) {
        document.webkitCancelFullScreen();
      }
    },
    true,
  );

  fullscreenElt.addEventListener('click', e => {
    e.preventDefault();
    if (
      !document.fullscreenElement &&
      !document.mozFullScreenElement &&
      !document.webkitFullscreenElement
    ) {
      if (consoleElt.requestFullscreen) {
        consoleElt.requestFullscreen();
      } else if (consoleElt.mozRequestFullscreen) {
        consoleElt.mozRequestFullscreen();
      } else if (consoleElt.webkitRequestFullscreen) {
        consoleElt.webkitRequestFullscreen();
      }
    } else if (document.cancelFullScreen) {
      document.cancelFullScreen();
    } else if (document.mozCancelFullScreen) {
      document.mozCancelFullScreen();
    } else if (document.webkitCancelFullScreen) {
      document.webkitCancelFullScreen();
    }
  });
}

/**
 * Generates a canvas according to the configuration passed as arguments.
 *
 * @param {string} containerId - The id of the conatiner HTML element.
 * @param {string} canvasId - The id of the canvas HTML element.
 * @param {string} fullscreenId - The id of the fullscreen switcher HTML element.
 * @param {boolean} createContainer - if true, will create the container and inject it into the body tag of the page.
 * @param {number} initialWidth - The initial width of the canvas.
 * @param {number} initialHeight - The initial height of the canvas.
 */
export function generateScreen(
  containerId: string,
  canvasId: string,
  fullscreenId: string,
  createContainer: boolean,
  initialWidth: number,
  initialHeight: number,
): void {
  const canvasElt = document.createElement('canvas');
  canvasElt.id = canvasId;
  canvasElt.classList.add('catana-screen-canvas');
  canvasElt.width = initialWidth;
  canvasElt.height = initialHeight;
  if (createContainer) {
    const containerElt = document.createElement('div');
    containerElt.classList.add('catana-screen');
    containerElt.id = containerId;
    containerElt.appendChild(canvasElt);
    document.body.appendChild(containerElt);
  } else {
    const containerElt = document.getElementById(containerId) as HTMLElement;
    containerElt.classList.add('catana-screen');
    containerElt.appendChild(canvasElt);
  }
  const fullscreenElt = document.createElement('span');
  fullscreenElt.id = fullscreenId;
  document.body.appendChild(fullscreenElt);
}
