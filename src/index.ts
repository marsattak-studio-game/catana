export { CatGameEngine } from './classes/CatGameEngine';
export { CatImage } from './classes/drawable/CatImage';
export { CatRect } from './classes/drawable/CatRect';
export { CatSprite } from './classes/drawable/CatSprite';
export { CatText } from './classes/drawable/CatText';
export { CatCircle } from './classes/drawable/CatCircle';

export { CatAScene } from './classes/scenes/CatAScene';

export { isTouchDevice } from './utils/screen/isTouchDevice';

export { CatAudioManager } from './classes/managers/CatAudioManager';

export { FONT_SIZE, FONT_TYPE } from './config/texts';
