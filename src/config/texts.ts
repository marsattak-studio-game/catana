const DEFAULT_FONT = '"Times New Roman", Times, serif' as const;

/**
 * Basic list of CSS Web Safe Font Combinations.
 */
const FONT_TYPE = {
  DEFAULT: DEFAULT_FONT,
  ARIAL: 'Arial, Helvetica, sans-serif',
  TIMES_NEW_ROMAN: '"Times New Roman", Times, serif',
  COURIER_NEW: '"Courier New", Courier, monospace',
} as const;

/**
 * Helper list of pixel size.
 */
const FONT_SIZE = {
  XXS: '20px',
  XS: '30px',
  SM: '40px',
  MD: '60px',
  LG: '80px',
} as const;

export { FONT_TYPE, FONT_SIZE };
