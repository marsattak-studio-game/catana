<div align="center">

# Catana

![catane-logo](https://gitlab.com/marsattak-studio-game/catana/-/raw/master/src/assets/img/catana-logo.png)

</div>

---

- [Description](#description)
- [Install](#install)
- [Usage](#usage)
- [Manual](#manual)

---

| version | **0.0.2** |
| ------- | --------- |


## Infos

> This framework has been created for educational purposes and personal projects.
> You can have fun with it, but for real projects,
> it is better to look at [more complete and proven frameworks](https://github.com/collections/javascript-game-engines).

---

## Description

Catana is a light and basic game framework for web browser.

It is written in javascript and supports typescript.

---

## Install

##### CDN

```js
<script
  type="text/javascript"
  src="https://unpkg.com/@msgame/catana@latest"></script>
```

##### NPM

```bash
npm i -S @msgame/catana
```

---

## Basic Usage

This code will create and run an instance of the game engine.

If everything works, it should display an example scene.

```js
import { CatGameEngine } from '@msgame/catana';

window.addEventListener('load', () => {
  const gameEngine = new CatGameEngine();
  gameEngine.run();
});
```

---

## Manual

You can see the get-started [here](https://gitlab.com/marsattak-studio-game/catana/-/blob/master/manual/get-started/README.md).

You can see the documentation [here](https://gitlab.com/marsattak-studio-game/catana/-/blob/master/manual/docs/README.md).

Or you can see some examples [here](https://gitlab.com/marsattak-studio-game/catana/-/blob/master/manual/examples/index.md).

<div align="center">

**Have fun.**

</div>
